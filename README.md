NodeJS wrapper for Docker REST API
=================================

NOTE: alpha status

A set of simple functions for accessing the Docker APRI.

These functions are asynchronous. Check the unit tests for examples on howto use them.
Be warned, it does take special attention to work with async functions.

Run `make doc` to generate the documentation.


Contributing
-----------

Run `make install` first. Then `make test` will run the unit tests.
Finally `make` will complile (lint and uglify).

Make sure that there are unit tests for everything. Please note that the tests
will return OK also when failing. Read the console output carefully for the 
test results.

`make clean` will clean things up.
