/*jshint -W054, evil:true, devel:true, browser:true*/

//
// Unit tests for the helper functions that are executed from node
//
// Run like this:
// >nodeunit helpers_test.js
//

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

// Unit test
//-------------------------------------------------------------------------------------------------
// Basic tests that just calls the functions to make sure that execute ok

exports['test_node_docker'] = {

  setUp: function(done) {
    // setup here
    this._async      = require('async');

    // the functions to test
    this._node_docker = require('../node-docker').create();
    this._helpers     = require('helpersjs').create();
    this._helpers.logging_threshold = this._helpers.logging.debug;

    // setup finished
    done();
  },

  'test images and containers': function(test) {

    this._helpers.logDebug('testing testing');

    // There should be X tests
    test.expect(1);

    this._helpers.logDebug('test: Start...');

    this._node_docker._imageID       = "test-imageID";
    this._node_docker._name          = "test-name";
    this._node_docker._containerID   = "test-containerID";

    this._async.series([
        // Delete the container and image if it already exists
        function(fn){ console.log("delete..."); this._node_docker.delete(fn); }.bind(this),
        function(fn){ console.log("import..."); this._node_docker.import(fn); }.bind(this),
        function(fn){ console.log("build...");  this._node_docker.build(fn); }.bind(this),
        function(fn){ console.log("create..."); this._node_docker.createContainer(fn); }.bind(this),
        function(fn){ console.log("start...");  this._node_docker.start(fn); }.bind(this),
        function(fn){ console.log("inspect...");this._node_docker.inspect(fn); }.bind(this),
        function(fn){ console.log("logs...");   this._node_docker.logs(fn); }.bind(this),

        // Dummy test
        function(fn) {
            test.equal(true,  this._helpers.isEmptyObject({}),      'isEmptyObject');
            fn(null, 'isEmptyObject');
        }.bind(this),

        function(fn) { 
            // All tests performed
            test.done();
            fn(null, 'test.done');
        }.bind(this),        

    ],
    function(err, results){
      this._helpers.logDebug('test: results of async functions - ' + results);
      this._helpers.logDebug('test: errors (if any) - ' + err);
    }.bind(this));

    this._helpers.logDebug('test: End of function, async processing will continue');

  }


/* 

TODO: Input for more tests

    switch (argv.cmd) {

        case "push":
            this.push();
            break;

        case "help":
            console.log('--cmd push --name=www.example.com --port=8080: webapp.tar in the current directory will be deployed to the cloud');
            console.log('--cmd status --name=XXX: show logs for container');
            console.log('--help: show this message');
            break;

        case "status":
            this.status();
            break;

        default:
            console.log('No such command: ' + argv.cmd);

    }
*/


};