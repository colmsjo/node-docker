(function(exports) {

// node-docker.js
//------------------------------
//
// 2013-10-29, Jonas Colmsjö
//
// Wrapper around the Docker.io REST API
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------
//
// In browsers (not a likely scenario):
//---------------
// <script type="text/javascript" src="https://raw.github.com/colmsjo/node-docker/master/node-docker.js"></script>
// ...
// var h = node-docker.create();
// h.test("Testing to use the plates library.", function(){
// ...
//
// In Node:
//---------------
// var node_docker = require('node-docker').create();


  "use strict";

  exports.create = function() {

    return {
 
      // Properties
      //==============

      _imageID       : "",
      _containerID   : "",
      _name          : "",
      _containerPort : null,  //previously undefined
      _use_export    : false,
      _settings      : {},
      _containers    : null,
      _helpers       : require('helpersjs').create(),


      // helpers
      //======================================================================

      _isset : function(a, message, dontexit){
        if (!this._helpers.isset(a)) {
          console.log(message);
          if(dontexit !== undefined && dontexit) {
            return false;
          } else {
            return false;
            // No good - process.exit();
          }        
        }
        this._helpers.logDebug('_isset: returning true ');
        return true;
      },


      // Docker functions
      //======================================================================

      _dockerRemoteAPI : function(options, funcResData, funcResEnd, funcReq, asyncCallback){

        var http =  require('http');

        if(options.hostname === undefined) {
          options.hostname = "localhost";
        }

        if(options.port === undefined) {
          options.port     = 4243;
        }

        var req = http.request(options, function(res) {
          res.setEncoding('utf8');

          res.on('data', funcResData.bind(this));

          if(funcResEnd !== null) {
            res.on('end', funcResEnd.bind(this));
          } else {
            res.on('end', function () {
              if(asyncCallback !== undefined) {
                asyncCallback();
              }
            }.bind(this));
          }
        }.bind(this));

        req.on('error', function(e) {
          if(asyncCallback !== undefined) {
            asyncCallback();
          }
          console.log("ERROR: Problem doing docker call!");
        }.bind());

        req.on('end', function(e) {
          // Doing nothing right now
          if(asyncCallback !== undefined) {
            asyncCallback();
          }
        }.bind(this));

        if(funcReq !== null) {
          funcReq(req);
        } else {
          req.end();
        }

      },


      // import
      //-------------------------------------------------------------------------------------------------
      //
      // Equivalent of: curl -H "Content-type: application/tar" --data-binary @webapp.tar http://localhost:4243/build
      //

      import : function(asyncCallback){
 
        var options = {
          path: '/images/create?fromSrc=-',
          method: 'POST',
          headers: {
            'Content-Type': 'application/tar',
          }
        };

        this._dockerRemoteAPI(options, 
          function(chunk) {
            this._imageID = JSON.parse(chunk).status;
          }.bind(this),
          function() {
            this._isset(this._imageID, 'Import failed! No image was created.');
            if(asyncCallback !== undefined) {
              asyncCallback(null, 'image:'+this._imageID);
            }      
          }.bind(this),
          function(req) {
            // write data to the http.ClientRequest (which is a stream) returned by http.request() 
            var fs = require('fs');

            var stream;
            try {
              stream = fs.createReadStream('webapp.export.tar');
            } catch (e) {
              console.log('ERROR: Could not open webapp.export.tar!');
              return;
            }
 
            // Close the request when the stream is closed
            stream.on('end', function() {
              req.end();
            }.bind(this));

            // send the data
            stream.pipe(req);
          }.bind(this),
        asyncCallback);
      },


      // build
      //-------------------------------------------------------------------------------------------------
      //
      // Equivalent of: curl -H "Content-type: application/tar" --data-binary @webapp.tar http://localhost:4243/build
      //

      build : function(asyncCallback){

        var options = {
          path: '/build?nocache',
          method: 'POST',
          headers: {
            'Content-Type': 'application/tar',
          }
        };

        this._dockerRemoteAPI(options, 
          function(chunk) {
            console.log('build: ' + chunk);

            // The last row looks like this 'Successfully built 3df239699c83'
            if (chunk.slice(0,18) === 'Successfully built') {
                this._imageID = chunk.slice(19,31);
            }
          }.bind(this),
          function() {
            this._isset(this._imageID, 'Build failed! No image was created.');
            if(asyncCallback !== undefined) {
              asyncCallback(null, 'image:'+this._imageID);
            }      
          }.bind(this),
          function(req) {
            // write data to the http.ClientRequest (which is a stream) returned by http.request() 
            var fs = require('fs');

            var stream;
            try {
              stream = fs.createReadStream('webapp.tar');
            } catch (e) {
              console.log('ERROR: Could not open webapp.tar!');
              return;
            }
 
            // Close the request when the stream is closed
            stream.on('end', function() {
              req.end();
            }.bind(this));

            // send the data
            stream.pipe(req);
          }.bind(this),
          asyncCallback);

          return this._imageID;
      },


      // createContainer
      //-------------------------------------------------------------------------------------------------
      //
      // create a container with the new image
      // curl -H "Content-Type: application/json" -d @create.json http://localhost:4243/containers/create
      // {"Id":"c6bfd6da99d3"}

      createContainer : function(asyncCallback){

        this._isset(this._imageID, 'createContainer: imageID not set');

        var container = {
         "Hostname":"",
         "User":"",
         "Memory":0,
         "MemorySwap":0,
         "AttachStdin":false,
         "AttachStdout":true,
         "AttachStderr":true,
         "PortSpecs":null,
         "Tty":false,
         "OpenStdin":false,
         "StdinOnce":false,
         "Env":null,
         "Dns":null,
         "Image":this._imageID,
         "Volumes":{},
         "VolumesFrom":""
        };

        var options = {
          path: '/containers/create',
          method: 'POST'
        };
        this._dockerRemoteAPI(options, 
          function(chunk) {
              // The result should look like this '{"Id":"c6bfd6da99d3"}'
              try {
                this._containerID = JSON.parse(chunk).Id;            
              } catch (e) {
                  console.log('Create container failed: '+chunk);
                  return false;          
              }
          }.bind(this),
          console.log, //null,
          function(req) {
              req.write(JSON.stringify(container));
              req.end();
          }.bind(this),
          asyncCallback
        );
     },


      // start
      //-------------------------------------------------------------------------------------------------
      //
      // Equivalent of: curl -H "Content-Type: application/json" -d @start.json http://localhost:4243/containers/c6bfd6da99d3/start
      //

      start : function(asyncCallback){

        this._isset(this._containerID, 'start: this._containerID not set');

        var binds = {
            "Binds":["/tmp:/tmp"]
        };

        var options = {
          path:     '/containers/'+this._containerID+'/start',
          method:   'POST'
        };

        this._dockerRemoteAPI(options, 
          function(chunk) {
            console.log('start: ' + chunk);
          }.bind(this),
          null,
          function(req) {
            req.write(JSON.stringify(binds));
            req.end();
          }.bind(this),
          asyncCallback
        );
      },


      // delete
      //-------------------------------------------------------------------------------------------------
      //
      // Equivalent of: curl -d '' http://localhost:4243/containers/c6bfd6da99d3/stop?t=10
      //

      delete : function(asyncCallback){

        // Get the container ID for the name
        if (!this._isset(this._containerID, "Container missing, can't delete", true)) {
          return (asyncCallback !== undefined) ?  asyncCallback() : false;
        }

        this._settings = this.inspect(this._containerID, 

          // stop the container
          function () {
            var options = {
              path:     '/containers/'+this._containerID+'/stop?t=10',
              method:   'POST'
            };

            this._dockerRemoteAPI(options, 
              function(chunk) { this._helpers.logDebug('delete: ' + chunk);}.bind(this),
              null, null,

              // Delete the container
              function() {
                var options = {
                  path:     '/containers/'+this._containerID+'?v=1',
                  method:   'DELETE'
                };

                this._dockerRemoteAPI(options, 
                  function(chunk) { this._helpers.logDebug('delete: ' + chunk); }.bind(this),
                  null, null,
                  
                  // Delete the image
                  function() {
                    var options = {
                      path:     '/images/'+this._settings.Image,
                      method:   'DELETE'
                    };

                    this._dockerRemoteAPI(options, 
                      function(chunk) { this._helpers.logDebug('delete: ' + chunk); }.bind(this),
                      null, null, asyncCallback);
                  }
                ); //_dockerRemoteAPI
              }
            ); //_dockerRemoteAPI
          }
        ); //inspect

      },


      // inspect - populate this._settings using containerID
      //-------------------------------------------------------------------------------------------------
      //
      // Equivalent of: curl -G http://localhost:4243/containers/c6bfd6da99d3/json
      //

      inspect : function(asyncCallback){

        if (!this._isset(this._containerID, 'inspect: containerID not set', true)) {
          return;
        }

        var options = {
          path:     '/containers/'+this._containerID+'/json',
          method:   'GET'
        };
 
        this._dockerRemoteAPI(options, 

          function(chunk) {
            try {
              this._settings = JSON.parse(chunk);
              this._imageID  = this._settings.Image;
            } catch (e) {
              console.log('inspect: error fetching data for - ' + this._containerID);
              return (asyncCallback !== undefined) ? asyncCallback() : false;
            }
          }.bind(this),
          null,
          null,
          asyncCallback
        );

        return this._settings;
      },


      // logs
      //-------------------------------------------------------------------------------------------------
      //
      // Get the logs of the started container (should show the current date since that's all the container does)
      // Equivalent of: curl -H "Content-Type: application/vnd.docker.raw-stream" -d '' "http://localhost:4243/containers/c6bfd6da99d3/attach?logs=1&stream=0&stdout=1"
      //

      logs : function(asyncCallback){
        this._isset(this._containerID, 'logs: this._containerID not set');

        var options = {
          path:     '/containers/'+this._containerID+'/attach?logs=1&stream=0&stdout=1',
          method:   'POST',
          headers: {
            'Content-Type': 'application/vnd.docker.raw-stream',
          }
        };

        this._dockerRemoteAPI(options, function(chunk) {
            console.log('logs: ' + chunk);
          },
          null,
          null,
          asyncCallback
        );
      },


      // status functions
      //-------------------------------------------------------------------------------------------------
      //
      // Show logs and settings
      //

      // status helper function
      // curl -G http://localhost:4243/containers/json
      containers : function(asyncCallback) {

        var options = {
          path:     '/containers/json',
          method:   'GET',
        };

        var prettyjson = require('prettyjson');

        this._dockerRemoteAPI(options, function(chunk) {
            this._containers = JSON.parse(chunk);

            this._containers.forEach(function(container) {
              var containerID = container.Id;
              this._inspect(containerID, function(){
                console.log("container: "+prettyjson.render(this._settings));
              });
            });

            console.log('containers: ' + prettyjson.render(this._containers));

          },
          null, null,
          asyncCallback
        );
      },

      status : function(containerID){

        var prettyjson = require('prettyjson');

        // List all containers
        if (containerID === undefined) {
          this.containers();
        } 

        // Show status for a specific container
        else {
          this.inspect(containerID,
            function() { this.logs(null); }.bind(this)
          );
        }
      }

    }; // return

  }; // exports.create

}(typeof exports === 'undefined' ? this['node-docker']={} : exports));
